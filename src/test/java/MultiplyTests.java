import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;



import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class MultiplyTests {
    public static WebDriver chromeDriver;
    static WebDriverWait wait;

    @BeforeClass
    static public void setup() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        chromeDriver = new ChromeDriver();
        wait = new WebDriverWait(chromeDriver, 15);
    }

    @Test
    public void multiplyTest() throws IOException {
        chromeDriver.get("http://www.anaesthetist.com/mnm/javascript/calc.htm");
        WebElement five = wait.until(ExpectedConditions.elementToBeClickable(By.name("five")));
        WebElement two = wait.until(ExpectedConditions.elementToBeClickable(By.name("two")));
        WebElement multiply = wait.until(ExpectedConditions.elementToBeClickable(By.name("mul")));
        WebElement result = wait.until(ExpectedConditions.elementToBeClickable(By.name("result")));
        WebElement display = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("Display")));


        five.click();
        multiply.click();
        two.click();
        result.click();
        TakesScreenshot screenshot = (TakesScreenshot) chromeDriver;
        File scr = screenshot.getScreenshotAs(OutputType.FILE);

        FileUtils.copyFile(scr, new File("screenshot.png"));

        Assert.assertEquals(10, Integer.parseInt(display.getAttribute("value")));
    }

    @AfterClass
    public static void clean(){
        chromeDriver.close();
    }
}
